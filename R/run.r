# Path conf
input_folder <- '~/git/datathon_dataset/'
output_folder <- '~/git/datathon_dataset/'

input_csv_person <- paste(input_folder, "PERSON.csv", sep="")
input_csv_location <- paste(input_folder, 'LOCATION.csv', sep="")
input_csv_visit_occurrence <- paste(input_folder, 'VISIT_OCCURRENCE.csv', sep="")
input_csv_procedure_occurrence <- paste(input_folder, 'PROCEDURE_OCCURRENCE.csv', sep="")
input_csv_condition_occurrence <- paste(input_folder, 'CONDITION_OCCURRENCE.csv', sep="")

# Installation package
install.packages("vioplot")

# Main algo called "myAlgo"
myAlgo <- function(input_csv_person, input_csv_location, input_csv_visit_occurrence,
            input_csv_procedure_occurrence, input_csv_condition_occurrence) {

	print("this is my super algo")

	duplicateDF <- c('1,2,3','4,5,6','7,8,9')

	# use to_csv to write the csv
    	# separator = ,
    	# no index, no header
	output_csv_result = paste(output_folder, 'RESULT.csv', sep="")
	write.table(duplicateDF, output_csv_result, sep=",", row.names=FALSE, col.names=FALSE) 
}

# hospitals will run your function call 'myAlgo', your fonction have to produce a csv
myAlgo(input_csv_person, input_csv_location, input_csv_visit_occurrence,
            input_csv_procedure_occurrence, input_csv_condition_occurrence)
