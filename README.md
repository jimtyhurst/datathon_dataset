# OVERVIEW

French hospitals (interCHU group) organize a [datathon](https://interchu.frama.io/website/) during two months. The result will be presented during Medinfo congress (August 25th - 30th). 

Interoperability and data access outside hospitals are limiting factors in the development of AI in medicine.

The Interchu group therefore proposes to use a synthetic data set transformed into a widely used common data model (OMOP) to develop algorithms without having direct access to the data.
Once developed, these algorithms will then be transmitted to partner hospitals for execution by local teams. The performance scores will then be returned to the participants.

# CHALLENGE
All details of a patient's hospitalization are kept by the hospital teams. In some cases, the patient is not well identified (name misprint, human error...) and as a result his history is fragmented into several medical records: this is the concept of duplicated patient. This could cause harm to patients and result in significant costs. In addition, these duplicated individuals affect the quality of data and the results of impact analysis algorithms. Currently, duplicate candidate patients are searched manually and the process could be improved by algorithms that identify them. Ultimately, the identified duplicate patients could be merged with a great benefit for care, cost and research.

The challenge is to provide the best opensource algorithm to identify duplicated patients in a hospital in a given database. 


# DATA

## OMOP Format
What is the OMOP Common Data Model (CDM)?
The OMOP Common Data Model allows for the systematic analysis of disparate observational databases. The concept behind this approach is to transform data contained within those databases into a common format (data model) as well as a common representation (terminologies, vocabularies, coding schemes), and then perform systematic analyses using a library of standard analytic routines that have been written based on the common format.

![OMOP-CDM shema](img/OMOP-CDM.png)

### Why do we need a CDM?
Observational databases differ in both purpose and design. Electronic Medical Records (EMR) are aimed at supporting clinical practice at the point of care, while administrative claims data are built for the insurance reimbursement processes. Each has been collected for a different purpose, resulting in different logical organizations and physical formats, and the terminologies used to describe the medicinal products and clinical conditions vary from source to source.
The CDM can accommodate both administrative claims and EHR, allowing users to generate evidence from a wide variety of sources. It would also support collaborative research across data sources worldwide, in addition to being manageable for data owners and useful for data users.

### Why use the OMOP CDM?
The Observational Medical Outcomes Partnership (OMOP) CDM, now in its version 6.0, offers a solution outperforming other approaches. OMOP demonstrates that disparate coding systems can be harmonized—with minimal information loss—to a standardized vocabulary.
Once a database has been converted to the OMOP CDM, evidence can be generated using standardized analytics tools. We at OHDSI are currently developing Open Source tools for data quality and characterization, medical product safety surveillance, comparative effectiveness, quality of care, and patient-level predictive modeling, but there are also other sources of such tools, some of them commercial.

## BUILD ALGORITHMS on synthetic dataset  (OMOP format) 
-  Synthetic dataset in OMOP format = synPUF contains 1000 patients available in OMOP v6.0 and come from the CMS 2008-2010 Data Entrepreneurs’ Synthetic Public Use File (DE-SynPUF).
[Information on the SynPUF data source is available here](https://www.cms.gov/Research-Statistics-Data-and-Systems/Downloadable-Public-Use-Files/SynPUFs/DE_Syn_PUF.html)
- allowed to build algorithms in OMOP format only
- [synPUF data visualization](http://www.ohdsi.org/web/atlas/#/datasources/SYNPUF1K/dashboard)
- Remark : as it is a synthetic dataset it doesn't provide duplicate patients! The distribution of variables could be false.

### csv format
- Header of the columns
- UTF8 character set
- Comma field separator

### 6 clinical csv files in OMOP format :
    - PERSON.csv
    - VISIT_OCCURRENCE.csv
    - LOCATION.csv
    - PROCEDURE_OCCURRENCE.csv
    - CONDITION_OCCURRENCE.csv
    - OBSERVATION.csv

### 1 extension vocabulary csv file in OMOP format :
    - CCAM_CONCEPT_EXTENSION.csv

This file provide only the french procedure terminology called CCAM. 
Terminologies are freely accessible here : omop-mapper.fgh.ovh (basic and extension concepts) or here : athena.ohdsi.org (only basic terminologies)

## REPLICATE ALGORITHMS on real life (OMOP format)
- French hospitals : Grenoble, Lille, Paris, Rennes, Toulouse
- Osiris Group
- DRESS (Direction de la recherche, des études, de l'évaluation et des statistiques)

## EXAMPLES of relevant concepts
- firstname, lastname, address, date of birth...
- This variable may be provided (or not) in the real dataset

# ALLOWED LANGUAGES
Only python and R are allowed. 
Your main function have to be called myAlgo. The main file have to be called run.py or run.r

R and python examples are provided in eponym folders.

# EVALUATION

## INPUT 
The input of algorithm is the 6 clinical csv files. 
Hospitals will locally generate the same csv files.

## OUTPUT
The output of the programme will be one csv file with one column. This column contains all the duplicated person_id
(person_id are provided in person.csv). 

The output csv is called RESULT.csv. It does not contain header.

Example:  patient 1,2,3 and 4,5,6 are duplicate patients.
The csv to provide is:
``` csv
    "1,2,3"
    "4,5,6"
```

# WHO
We aim to bring together clinicians, data scientists and innovators in healthcare to address current problems in healthcare with data analytics technologies.
You want to learn medical interoperability on real-life dataset. You have a personal computer and a little time in July and August. This challenge is for you!

# PRICE
For the glory

# SUBMISSION TIMELINE
- Start Date:  10/07/2019
- OMOP-CDM presentation and challenge presentation: 22/07/2019
- Team submission Deadline: 30/07/2019
- first algorithm submission Deadline:  01/08/2019 (once a day)
- final algorithm submission Deadline:  20/08/2019

# RESOURCES
- [forum](interchu.zulipchat.com) : forum for personal registration and team submission
- [omop wiki](https://github.com/OHDSI/CommonDataModel/wiki)
- online terminologies' explorer : [omop-mapper](omop-mapper.fgh.ovh) or [athena](athena.ohdsi.org)
- [synPUF data visualization](http://www.ohdsi.org/web/atlas/#/datasources/SYNPUF1K/dashboard)
